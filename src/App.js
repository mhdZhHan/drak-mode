import { Content } from "./components/screens/Content"
import Toggle from "./components/includes/Toggle"
import { Global, lightTheme, darkTheme } from "./Global"
import { useDarkMode } from "./hooks/useDarkMode"
import styled, { ThemeProvider } from "styled-components"

function App() {
  const [theme, toggleTheme] = useDarkMode()
  const themeMode = theme === 'light' ? lightTheme : darkTheme
  return (
    <ThemeProvider theme={themeMode}>
        <Container>
            <Global />
            <Toggle theme={theme} toggleTheme={toggleTheme} />
            <Content />
        </Container>
    </ThemeProvider>
  )
}

const Container = styled.div`
  max-width: 60%;
  margin: 8rem auto 0;
`;
export default App
