import { createGlobalStyle } from "styled-components";

export const Global = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css2?family=Ubuntu&display=swap');

  body {
      background: ${({ theme })=>theme.body};
      color: ${({ theme })=>theme.text};
      transition: all .5s linear;
      font-family: 'Ubuntu', sans-serif;
  }

  .btn-primary {
      background: ${({ theme })=>theme.primary};
      color: ${({ theme })=>theme.body};
      border: 0;
      outline: none;
      padding: 0.5rem 1.5rem;
      border-radius: .4rem;
      font-size: 18px;
      font-weight: 700;
  }

  p {
      line-height: 1.5rem;
  }

  svg {
      font-size: 30px;
      width: 30px;
      height: 30px;
      cursor: pointer;
  }
`;

export const lightTheme = {
    body: '#fff',
    text: '#121212',
    primary: '#6200ee',
}

export const darkTheme = {
    body: '#000000d5',
    text: '#fff',
    primary: '#bb86fc',
}